<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemMasters extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('item_masters', function(Blueprint $table)
		{
			$table->increments('id');
			$table->smallInteger('rarity')->unisgned; //0=Common, 1=uncommon , 2=rare, 3=Super Rare
			$table->string('name', 256);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('item_masters');
	}

}
