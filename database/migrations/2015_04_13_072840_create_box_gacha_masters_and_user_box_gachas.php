<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoxGachaMastersAndUserBoxGachas extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('box_gacha_masters', function(Blueprint $table)
		{
			$table->increments('id');
			$table->Integer('gacha_id')->unisgned;
			$table->Integer('item_id')->unisgned;
			$table->smallInteger('max_number')->unisgned();
			$table->timestamps();
		});
		Schema::create('user_box_gachas', function(Blueprint $table)
		{
			$table->increments('id');
			$table->Integer('user_id')->unisgned;
			$table->Integer('gacha_id')->unisgned;
			$table->Integer('item_id')->unisgned;
			$table->smallInteger('draw_number')->unisgned()->dafault(0);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('box_gacha_masters');
		Schema::drop('user_box_gachas');
	}

}
