<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserGacha extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_gachas', function(Blueprint $table)
		{
			$table->increments('id');
			$table->Integer('user_id')->unisgned;
			$table->Integer('gacha_id')->unisgned;
			$table->smallInteger('draw_times_for_free')->unisgned()->default(0);
			$table->timestamp('last_draw_time_for_free')->nullable();
			$table->smallInteger('draw_times_for_paid')->unisgned()->default(0);
			$table->timestamp('last_draw_time_for_paid')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_gachas');
	}

}
