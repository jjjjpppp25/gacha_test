<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGachaMaster extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('gacha_masters', function(Blueprint $table)
		{
			$table->increments('id');
			$table->smallInteger('type')->unisgned; //1 = Normal, 2 = Expensive, 3 = Box
			$table->string('name', 256);
			$table->smallInteger('price')->unisgned();
			$table->smallInteger('free_draw')->unisgned();
			$table->smallInteger('free_draw_period_days')->unisgned();
			$table->smallInteger('free_draw_period_hours')->unisgned();
			$table->smallInteger('probabilityes_common')->unsigned();
			$table->smallInteger('probabilityes_uncommon')->unsigned();
			$table->smallInteger('probabilityes_rare')->unsigned();
			$table->smallInteger('probabilityes_super_rare')->unsigned();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('gacha_masters');
	}

}
