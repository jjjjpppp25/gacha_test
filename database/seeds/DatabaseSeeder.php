<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Model\GachaMaster;
use App\Model\User;
use App\Model\UserGacha;
use App\Model\BoxGachaMaster;
use App\Model\UserBoxGacha;
use App\Model\ItemMaster;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call('GachaMastersTableSeeder');
		$this->call('UserTableSeeder');
		$this->call('UserGachaTableSeeder');
		$this->call('ItemMastersTableSeeder');
		$this->call('BoxGachaMastersTableSeeder');
		$this->call('UserBoxGachasTableSeeder');
		$this->command->info('gacha_master table seedes');
	}

}

class ItemMastersTableSeeder extends Seeder {
	public function run()
	{
		DB::table('item_masters')->delete();
		ItemMaster::create(['rarity' => 0,'name' => "item_1"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_2"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_3"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_4"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_5"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_6"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_7"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_8"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_9"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_10"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_11"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_12"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_13"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_14"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_15"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_16"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_17"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_18"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_19"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_20"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_21"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_22"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_23"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_24"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_25"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_26"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_27"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_28"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_29"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_30"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_31"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_32"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_33"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_34"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_35"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_36"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_37"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_38"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_39"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_40"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_41"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_42"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_43"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_44"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_45"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_46"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_47"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_48"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_49"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_50"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_51"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_52"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_53"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_54"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_55"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_56"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_57"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_58"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_59"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_60"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_61"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_62"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_63"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_64"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_65"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_66"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_67"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_68"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_69"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_70"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_71"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_72"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_73"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_74"]);
		ItemMaster::create(['rarity' => 0,'name' => "item_75"]);
		ItemMaster::create(['rarity' => 1,'name' => "item_76"]);
		ItemMaster::create(['rarity' => 1,'name' => "item_77"]);
		ItemMaster::create(['rarity' => 1,'name' => "item_78"]);
		ItemMaster::create(['rarity' => 1,'name' => "item_79"]);
		ItemMaster::create(['rarity' => 1,'name' => "item_80"]);
		ItemMaster::create(['rarity' => 1,'name' => "item_81"]);
		ItemMaster::create(['rarity' => 1,'name' => "item_82"]);
		ItemMaster::create(['rarity' => 1,'name' => "item_83"]);
		ItemMaster::create(['rarity' => 1,'name' => "item_84"]);
		ItemMaster::create(['rarity' => 1,'name' => "item_85"]);
		ItemMaster::create(['rarity' => 1,'name' => "item_86"]);
		ItemMaster::create(['rarity' => 1,'name' => "item_87"]);
		ItemMaster::create(['rarity' => 1,'name' => "item_88"]);
		ItemMaster::create(['rarity' => 1,'name' => "item_89"]);
		ItemMaster::create(['rarity' => 1,'name' => "item_90"]);
		ItemMaster::create(['rarity' => 2,'name' => "item_91"]);
		ItemMaster::create(['rarity' => 2,'name' => "item_92"]);
		ItemMaster::create(['rarity' => 2,'name' => "item_93"]);
		ItemMaster::create(['rarity' => 2,'name' => "item_94"]);
		ItemMaster::create(['rarity' => 2,'name' => "item_95"]);
		ItemMaster::create(['rarity' => 2,'name' => "item_96"]);
		ItemMaster::create(['rarity' => 3,'name' => "item_97"]);
		ItemMaster::create(['rarity' => 3,'name' => "item_98"]);
		ItemMaster::create(['rarity' => 3,'name' => "item_99"]);
		ItemMaster::create(['rarity' => 3,'name' => "item_100"]);
	}
}

class BoxGachaMastersTableSeeder extends Seeder {
	public function run()
	{
		DB::table('box_gacha_masters')->delete();
		BoxGachaMaster::create([
			'id' => 1,
			'gacha_id' => 3,
			'item_id' => 100,
			'max_number' => 1,
		]);
		BoxGachaMaster::create([
			'id' => 2,
			'gacha_id' => 3,
			'item_id' => 95,
			'max_number' => 5,
		]);
		BoxGachaMaster::create([
			'id' => 3,
			'gacha_id' => 3,
			'item_id' => 89,
			'max_number' => 10,
		]);
		BoxGachaMaster::create([
			'id' => 4,
			'gacha_id' => 3,
			'item_id' => 70,
			'max_number' => 50,
		]);

	}
}

class UserBoxGachasTableSeeder extends Seeder {
	public function run()
	{
		DB::table('user_box_gachas')->delete();
		//UserBoxGacha::create([
		//	'id' => 1,
		//	'user_id' => "user_a",
		//	'gacha_id' => 3,
		//	'item_id' => 100,
		//	'draw_number' => 1,
		//]);
		//UserBoxGacha::create([
		//	'id' => 2,
		//	'user_id' => "user_a",
		//	'gacha_id' => 3,
		//	'item_id' => 95,
		//	'draw_number' => 1,
		//]);
		//UserBoxGacha::create([
		//	'id' => 3,
		//	'user_id' => "user_a",
		//	'gacha_id' => 3,
		//	'item_id' => 89,
		//	'draw_number' => 5,
		//]);
		//UserBoxGacha::create([
		//	'id' => 4,
		//	'user_id' => "user_a",
		//	'gacha_id' => 3,
		//	'item_id' => 70,
		//	'draw_number' => 25,
		//]);

	}
}

class UserTableSeeder extends Seeder {
	public function run()
	{
		DB::table('users')->delete();
		User::create([
			'id' => 1,
			'user_id' => "user_a",
			'password' => bcrypt("user_a"),
			'coin' => 99999,
		]);
	}
}

class UserGachaTableSeeder extends Seeder {
	public function run()
	{
		//date_default_timezone_set("Asia/Singapore");
		DB::table('user_gachas')->delete();
		UserGacha::create([
			'id' => 1,
			'user_id' => "user_a",
			'gacha_id' => 1,
			'draw_times_for_free' => 0,
			'last_draw_time_for_free' => date("Y-m-d H:i:s"),
			'draw_times_for_paid' => 0,
			'last_draw_time_for_paid' => date("Y-m-d H:i:s"),
		]);
		UserGacha::create([
			'id' => 2,
			'user_id' => "user_a",
			'gacha_id' => 2,
			'draw_times_for_free' => 0,
			'last_draw_time_for_free' => date("Y-m-d H:i:s"),
			'draw_times_for_paid' => 0,
			'last_draw_time_for_paid' => date("Y-m-d H:i:s"),
		]);
	}
}

class GachaMastersTableSeeder extends Seeder {

	public function run()
	{
		// Normal Gacha
		DB::table('gacha_masters')->delete();
		GachaMaster::create([
			'id' => 1,
			'type' => 1,
			'name' => 'Normal Gacha',
			'price' => 100,
			'free_draw' => 1,
			'free_draw_period_days' => 0,
			'free_draw_period_hours' => 1,
			'probabilityes_common' => 70,
			'probabilityes_uncommon' => 25,
			'probabilityes_rare' => 4,
			'probabilityes_super_rare' => 1
		]);

		// Expensive Gacha
		GachaMaster::create([
			'id' => 2,
			'type' => 2,
			'name' => 'Expensive Gacha',
			'price' => 1000,
			'free_draw' => 1,
			'free_draw_period_days' => 1,
			'free_draw_period_hours' => 0,
			'probabilityes_common' => 10,
			'probabilityes_uncommon' => 50,
			'probabilityes_rare' => 30,
			'probabilityes_super_rare' => 10
		]);

		// Box Gacha
		GachaMaster::create([
			'id' => 3,
			'type' => 3,
			'name' => 'Box Gacha',
			'price' => 500,
			'free_draw' => 0,
			'free_draw_period_days' => 0,
			'free_draw_period_hours' => 0,
			'probabilityes_common' => 55,
			'probabilityes_uncommon' => 25,
			'probabilityes_rare' => 15,
			'probabilityes_super_rare' => 5
		]);

	}
}

