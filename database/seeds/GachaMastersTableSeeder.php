<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Model\GachaMaster; 

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		//Model::unguard();

		$this->call('GachaMastersTableSeeder');
		$this->command->info('gacha_master table seedes');
	}

}

class GachaMastersTableSeeder extends Seeder {

	public function run()
	{
		DB::table('gacha_masters')->delete();
		GachaMasters::create('type' => 1, 'name' => 'Normal Gacha', 'price' => 100, 'free_draw' => 1, 'free_draw_period_days' => 0, 'free_draw_period_hours' => 1, 'probabilityes_common' => 70, 'probabilityes_uncommon' => 25, 'probabilityes_rare' => 4, 'probabilityes_super_rare' => 1);

		
	}
}


