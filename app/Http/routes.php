<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



// authrization
Route::post('users', 'AuthController_v2@register');
Route::post('users/auth', 'AuthController_v2@authenticate');
Route::get('users/logout', 'AuthController_v2@logout');
Route::get('users/{user_id}', 'AuthController_v2@get_login_user_information');

// gacha
Route::get('gachas', 'GachaController@index');
Route::put('gachas/draw/{gacha_id}', 'GachaController@draw_gacha');
Route::get('gachas/draw/{gacha_id}', 'GachaController@draw_gacha');

// inventry
Route::get('inventories', 'InventoryController@index');


#Route::controllers([
#	'auth' => 'Auth\AuthController',
#	'password' => 'Auth\PasswordController',
#]);
