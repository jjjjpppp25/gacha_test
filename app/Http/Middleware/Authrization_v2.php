<?php namespace App\Http\Middleware;

use Closure;
use Auth;
use Log;

class Authrization_v2 {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		Log::debug($request->path());

		if (!in_array($request->path(), array('users','users/auth'))) {
			if (!Auth::check()) return response()->json(['code' => 103, 'message' => 'fail user haven\'t login'], 400);
		}

		return $next($request);
	}

}
