<?php namespace App\Http\Controllers;

use Auth;
use Log;
use App\Model\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class AuthController_v2 extends Controller {

	/**
	* do authrization
	*
	* @return Response
	*/
	public function authenticate(Request $request)
	{
		if (Auth::check()) Log::debug('Auth data', ['info' => 'True']);

		$user_id = $request->input('user_id');
		$password = $request->input('password');

		Log::debug('Auth data', ['user_id' => $user_id, 'password' => $password]);

		if (Auth::attempt(['user_id' => $user_id, 'password' => $password],true))
		{
			return response()->json(['message' => 'login success'], 200);
		}
		return response()->json(['code' => 102, 'message' => 'login faile'], 400);
	}

	public function register(Request $request)
	{

		$user_id = $request->input('user_id');
		$password = $request->input('password');

		//return var_dump([$user_id, $password]);
		$user = User::where('user_id', $user_id)->first();
		Log::debug(__CLASS__.__LINE__, [$user]);


		if (!empty($user)){
			return response()->json(['code' => 101, 'message' => 'this user name is already exists. please use other name'], 409);
		}

		$new_user = new User;
		$new_user->user_id = $user_id;
		$new_user->password = bcrypt($password);
		$new_user->coin = 5000;
		$new_user->save();

		return response()->json(['message' => 'registration success'], 200);
	}

	public function logout(Request $request)
	{
		Log::debug('Log out user', json_decode(json_encode($request->user()),true));
		Auth::logout();
		return response()->json(['message' => 'logout success'], 200);
	}

	public function get_login_user_information(Request $request, $user_id)
	{
		return response()->json(json_encode($request->user()), 200);
	}
}
