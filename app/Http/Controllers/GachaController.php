<?php namespace App\Http\Controllers;

use Log;
use App\Model\User;
use App\Model\GachaMaster;
use App\Services\GachaLogic;
use App\Services\BoxGachaLogic;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class GachaController extends Controller {

	protected $gacha_masters;
	protected $gacha_logic;

	function __construct(Request $request){
		$this->gacha_masters = GachaMaster::all();
	}
	/**
	*
	* @return Response
	*/
	public function index(Request $request)
	{
		$return_data = array();
		$return_data[] = ["user_name" => $request->user()->user_id, "coin" => $request->user()->coin];

		// create return data
		foreach ($this->gacha_masters as $key => $gacha_master) {
			$this->gacha_logic = $this->create_gacha_logic($request->user(), $gacha_master->type);
			$this->gacha_logic->set_gacha_master($gacha_master);
			$this->gacha_logic->reset_gacha();
			$gacha_master->is_draw_free = $this->gacha_logic->is_draw_gacha_for_free($gacha_master['id']);
			if ($gacha_master->type == GachaMaster::GACHA_TYPE_BOX ) {
				$gacha_master->box_info = $this->gacha_logic->get_box_information();
			}
			$return_data[] = $gacha_master;
		}
		Log::debug(__CLASS__.__LINE__, $return_data);

		return response()->json(json_encode( $return_data), 200);
	}

	public function draw_gacha(Request $request, $gacha_id) {

		//  gacha is not exists
		$gacha_master = $this->gacha_masters->where('id',$gacha_id)->first();
		if ( empty($gacha_master)){
			return response()->json(['code' => 104, 'message' => 'Gacha is not exists'], 400);
		};

		// free draw check
		$this->gacha_logic = $this->create_gacha_logic($request->user(), $gacha_master->type);
		$this->gacha_logic->set_gacha_master($gacha_master);
		if ($request->is_free == true && $this->gacha_logic->is_draw_gacha_for_free($gacha_id) == false){
			return response()->json(['code' => 105, 'message' => 'this gacha can not draw without spend coin'], 400);
		}

		// draw gacha (return item, register item to user_item and update user_gacha);
		try {
			$items = $this->gacha_logic->draw_gacha($gacha_id, $request->is_free);
		} catch (Exception $e) {
			return response()->json(['code' => 106, 'message' => $e->getMessage()], 400);
		}

		return response()->json(json_encode($items), 200);
	}

	private function create_gacha_logic($user,$type){
		Log::debug([__CLASS__ + __LINE__, ' type ', $type]);
		if ($type == GachaMaster::GACHA_TYPE_BOX) {
			return new BoxGachaLogic($user);
		}
		return new GachaLogic($user);
	}

}
