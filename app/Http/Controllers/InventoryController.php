<?php namespace App\Http\Controllers;

use Log;
use App\Model\User;
use App\Model\UserGacha;
use App\Model\GachaMaster;
use App\Model\ItemMaster;
use App\Model\UserItem;
use App\Services\GachaLogic;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class InventoryController extends Controller {


	public function index(Request $request)
	{
		$item_masters = ItemMaster::All();
		$user_items = UserItem::where('user_id', $request->user()->user_id)->get();

		$return_result = array();
		foreach($user_items as $key => $user_item) {
			$item_master = $item_masters->get($user_item->item_id -1);
			$user_item->name = $item_master->name;
			$user_item->rarity = $item_master->rarity;
			$return_result[] = $user_item;
		}
		return response()->json(json_encode($return_result), 200);
	}
}
