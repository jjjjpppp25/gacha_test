<?php namespace App\Services;

use Log;
use App\Model\User;
use App\Model\UserGacha;
use App\Model\ItemMaster;
use App\Model\UserItem;
use App\Model\BoxGachaMaster;
use App\Model\UserBoxGacha;
use Datetime;

class GachaLogic {

	protected $user;
	protected $gachamaster;

	function __construct ($user) {
		$this->user = $user;
	}
	public function set_gacha_master($gachamaster){
		$this->gachamaster = $gachamaster;
	}

	/**
	*
	* @return Response
	*/
	public function is_draw_gacha_for_free($gacha_id)
	{
		$user_gacha = UserGacha::where('user_id', $this->user['user_id'])->where('gacha_id', $gacha_id)->first();
		Log::debug(__CLASS__.__LINE__, array($user_gacha));

		// not allow free draw
		if ($this->gachamaster['free_draw'] < 1){
			return false;
		}

		// first time draw
		if ( empty($user_gacha) ){
			Log::debug(__CLASS__.__LINE__, array("true"));
			return true;
		}

		$datetime_last_draw = new Datetime($user_gacha['last_draw_time_for_free']);
		$free_period_day = $this->gachamaster['free_draw_period_days'];
		$free_period_hours = $this->gachamaster['free_draw_period_hours'];

		Log::debug(__CLASS__.__LINE__, array($free_period_day." days ", $free_period_hours . " hours"));

		$datetime_recover_date = clone $datetime_last_draw;
		$days_addition = '+ ' . $free_period_day . ' days ';
		$hours_addition ='+ ' . $free_period_hours . ' hours ';
		$datetime_recover_date = $datetime_recover_date->modify($days_addition. $hours_addition);
		Log::debug(__CLASS__.__LINE__, array("datetime_recover_date", $datetime_recover_date));
		Log::debug(__CLASS__.__LINE__, array("corrent_date", new Datetime()));

		if ( new Datetime() >= $datetime_recover_date ) {
				Log::debug(__CLASS__.__LINE__, array("return true"));
				return true;
		}
		return false;
	}

	public function draw_gacha($gacha_id, $is_free_draw) {
		// Todo Transaction and error
		// get item
		$pickuped_item = $this->pick_up_item_by_probabirity();
		// insert to user_item
		$user_item = new UserItem();
		$user_item->user_id = $this->user->user_id;
		$user_item->item_id = $pickuped_item->id;
		$user_item->save();

		// update user_gacha
		$user_gacha = UserGacha::where('user_id',$this->user->user_id)->where('gacha_id',$this->gachamaster->id)->first();
		if (is_null( $user_gacha)) {
				$user_gacha = New UserGacha();
				$user_gacha->user_id = $this->user->user_id;
				$user_gacha->gacha_id = $gacha_id;
		}

		if ($is_free_draw) {
			$user_gacha->draw_times_for_free = $user_gacha->draw_times_for_free + 1;
			$user_gacha->last_draw_time_for_free = date("Y-m-d H:i:s");
		} else {
			$user_gacha->draw_times_for_paid = $user_gacha->draw_times_for_paid + 1;
			$user_gacha->last_draw_time_for_paid = date("Y-m-d H:i:s");
			// update user.coin
			$this->user->coin = $this->user->coin - $this->gachamaster->price;
			$this->user->save();
		}
		$user_gacha->save();


		return $pickuped_item;
	}

	protected function pick_up_item_by_probabirity() {
		$rarity = $this->get_rarity();
		Log::debug(__CLASS__.__LINE__, array("get rearity", $rarity));
		$pickuped_item = $this->get_item($rarity);
		Log::debug(__CLASS__.__LINE__, array("pick up item", $pickuped_item));

		return $pickuped_item;
	}

	public function get_box_information(){
		// get data from box_gacha_masters
		$box_gacha_masters = BoxGachaMaster::where('gacha_id', $this->gachamaster->id)->get();
		// get data from user_box_gachas
		$user_box_gachas = UserBoxGacha::where('user_id', $this->user->user_id)->where('gacha_id', $this->gachamaster->id)->get();
		//var_dump($user_box_gachas);

		// create data
		$return_data = array();
		foreach($box_gacha_masters as $key => $box_gacha_master){
			$user_box_gacha = $user_box_gachas->where("item_id", $box_gacha_master->item_id)->first();
			//if (is_null($user_box_gacha)) $user_box_gacha->draw_number = 0;
			//var_dump($user_box_gacha);
			$item_master = ItemMaster::where("id", $box_gacha_master->item_id)->first();
			$draw_number = is_null($user_box_gacha) ? 0 :$user_box_gacha->draw_number;
			$return_data[] = [
				"item_id" => $item_master->id,
				"item_name" => $item_master->name,
				"item_rarity" => $item_master->rarity,
				"max_draw" => $box_gacha_master->max_number,
				"remain_number" => $box_gacha_master->max_number - $draw_number,
				];
		}

		return $return_data;;
	}

	protected function get_item($rarity) {
		$items = ItemMaster::where('rarity' ,$rarity)->get();
		return $items->random(1);
	}

	protected function get_rarity($is_get_common=true, $is_get_uncommon=true, $is_get_rare=true, $is_get_super_rare=true ){
		$rarity1 = $is_get_common ? $this->gachamaster['probabilityes_common'] : 0;
		$rarity2 = $is_get_uncommon ? $this->gachamaster['probabilityes_uncommon'] : 0;
		$rarity3 = $is_get_rare ? $this->gachamaster['probabilityes_rare'] : 0;
		$rarity4 = $is_get_super_rare ? $this->gachamaster['probabilityes_super_rare'] : 0;

		Log::debug(__CLASS__.__LINE__, array($rarity1,$rarity2,$rarity3,$rarity4));

		$rarities= array($rarity1, $rarity2, $rarity3, $rarity4);
		$sum = array_sum($rarities);
		$total = 0;
		$rand = rand(1, $sum);

		foreach($rarities as $key => $value) {
			$total += $value;
			if ($rand <= $total) {
				$result = $key;
				break;
			}
		}
		return $result;
	}

	public function reset_gacha(){
		return;
	}
}
