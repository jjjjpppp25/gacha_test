<?php namespace App\Services;

use Log;
use App\Model\User;
use App\Model\UserGacha;
use App\Model\ItemMaster;
use App\Model\UserItem;
use App\Model\BoxGachaMaster;
use App\Model\UserBoxGacha;
use App\Model\GachaMaster;
use Datetime;
use Exception;
use DB;

class BoxGachaLogic extends GachaLogic {

	Const BOX_RESET_TIME = "00:00:00";

	function __construct ($user) {
		parent::__construct($user);
	}

	protected function pick_up_item_by_probabirity() {

		$box_infomations = $this->get_box_information();

		$remains = array(0,0,0,0);
		foreach($box_infomations as $key => $item){
			$remains[$item['item_rarity']] += $item['remain_number'];
		}
		//var_dump($remains);

		// box reset

		// all raritys are 0 return error
		if($remains[GachaMaster::RARITY_COMMON] == 0 &&
			$remains[GachaMaster::RARITY_UNCOMMON] == 0 &&
			$remains[GachaMaster::RARITY_RARE] == 0 &&
			$remains[GachaMaster::RARITY_SUPER_RARE] == 0
		) {
			throw new Exception("Box is empty");
		}

		$rarity = $this->get_rarity($remains[GachaMaster::RARITY_COMMON],
			$remains[GachaMaster::RARITY_UNCOMMON],
			$remains[GachaMaster::RARITY_RARE],
			$remains[GachaMaster::RARITY_SUPER_RARE]
		);
		Log::debug(__CLASS__.__LINE__, array("get rearity", $rarity));
		$pickuped_item = $this->get_item($rarity);
		Log::debug(__CLASS__.__LINE__, array("pick up item", $pickuped_item));

		// update user_box_gacha
		$user_box_gacha = UserBoxGacha::where('user_id',$this->user->user_id)->where('gacha_id',$this->gachamaster->id)->where('item_id',$pickuped_item->id)->first();
		if (is_null( $user_box_gacha)) {
			$user_box_gacha = New UserBoxGacha();
			$user_box_gacha->user_id = $this->user->user_id;
			$user_box_gacha->gacha_id = $this->gachamaster->id;
			$user_box_gacha->item_id = $pickuped_item->id;
			$user_box_gacha->draw_number = 1;
		}else {
			$user_box_gacha->item_id = $pickuped_item->id;
			$user_box_gacha->draw_number += 1;
		}
		$user_box_gacha->save();


		return $pickuped_item;
	}

	protected function get_item($rarity) {
		$box_gacha_masters = BoxGachaMaster::where('gacha_id', $this->gachamaster->id)->get()->toArray();
		$drawble_item_ids = [];
		foreach($box_gacha_masters as $key => $box_gacha_master) {
			$drawble_item_ids[] = $box_gacha_master['item_id'];
		}
		//var_dump($drawble_item_ids);
		$items = ItemMaster::where('rarity',$rarity)->whereIn('id',$drawble_item_ids)->get();
		return $items->random(1);
	}

	public function reset_gacha(){
		$user_gacha = UserGacha::where("user_id",$this->user->user_id)->where("gacha_id",$this->gachamaster->id)->first();
		if (empty($user_gacha)) return; // if user haven't drawn this gacha. don't need to reset.

		$lastdraw_time = new Datetime($user_gacha->last_draw_time_for_paid);
		$reset_time = clone $lastdraw_time;
		$reset_time->modify(' + 1day ' . BoxGachaLogic::BOX_RESET_TIME);
		$now = new Datetime();

		Log::debug(__CLASS__.__LINE__, array("now",$now,"reset_time",$reset_time));
		if ($now >= $reset_time) {
			DB::update('update user_box_gachas set draw_number = 0 where user_id = ? and gacha_id = ?', [$this->user->user_id, $this->gachamaster->id]);
		}
		return;
	}
}
