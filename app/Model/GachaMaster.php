<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class GachaMaster extends Model  {

	// rarities
	const RARITY_COMMON = 0;
	const RARITY_UNCOMMON = 1;
	const RARITY_RARE = 2;
	const RARITY_SUPER_RARE =3;

	const GACHA_TYPE_BOX = 3;

}
